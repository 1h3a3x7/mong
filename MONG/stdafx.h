﻿// stdafx.h: archivo de inclusión de los archivos de inclusión estándar del sistema,
// o archivos de inclusión específicos de un proyecto utilizados frecuentemente, pero
// son rara vez modificados
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Excluir material rara vez utilizado de encabezados de Windows
// Archivos de encabezado de Windows
#include <windows.h>

// Archivos de encabezado en tiempo de ejecución de C
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include "wke.h"
#include <winnt.h>
#include "hidapi.h"
#include "Shlwapi.h"
#include "vendor\physfs\physfs.h"

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define LOG(...) Log("Trace: %s [%s](%d)",__FUNCTION__,__FILENAME__,__LINE__); if (#__VA_ARGS__>0) Log(__VA_ARGS__)
#define LOGW(...) Log("Trace: %s [%s](%d)",__FUNCTION__,__FILENAME__,__LINE__); if (#__VA_ARGS__>0) Logw(__VA_ARGS__)

