del "www\resources.zip"
call "vendor\7z.exe" a -r -tzip "www\resources.zip" ".\www\*" -mx9
call "vendor\objcopy.exe" --prefix-symbol=_ --rename-section .data=.rdata,CONTENTS,ALLOC,LOAD,READONLY,DATA --input-target binary --output-target pe-i386 --binary-architecture i386 "www\resources.zip" "%resources.lib"
EXIT 0