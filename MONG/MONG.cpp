﻿// MONG.cpp : Define el punto de entrada de la aplicación.
//

#include "stdafx.h"
#include "MONG.h"

#define MAX_LOADSTRING 100
// Variables globales:
HINSTANCE hInst;
WCHAR szTitle[MAX_LOADSTRING];
WCHAR szWindowClass[MAX_LOADSTRING];
char _[1] = { 0 };
wchar_t L_[1] = { 0 };
wkeWebView window;
HWND g_hWnd;
wchar_t gPath[MAX_PATH];

// Declaraciones de funciones adelantadas incluidas en este módulo de código:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void Log(const char * szFormat, ...);
void Logw(const wchar_t * szFormat, ...);
bool startsWith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
        lenstr = strlen(str);
    return lenstr < lenpre ? false : memcmp(pre, str, lenpre) == 0;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPWSTR    lpCmdLine,
    _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Colocar código aquí.

    // Inicializar cadenas globales
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MONG, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
    if (hid_init() != 0) {
        LOG("cant load hid");
        return FALSE;
    }

    PHYSFS_init(NULL);
    //PHYSFS_addToSearchPath("resources.zip", 1);
    PHYSFS_addToSearchPath("resources", 1);
    //_binary_resources_zip_start
    int siz = _binary_www_resources_zip_end - _binary_www_resources_zip_start;
    if (!PHYSFS_mountMemory(_binary_www_resources_zip_start, siz, NULL, "resources.zip", "/", 1)) {
        LOG("mount error code %d", PHYSFS_getLastErrorCode());
        return FALSE;
    }

    // Realizar la inicialización de la aplicación:
    if (!InitInstance(hInstance, nCmdShow))
        return FALSE;

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MONG));

    MSG msg;

    // Bucle principal de mensajes:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    hid_exit();
    PHYSFS_deinit();
    return /*(int) msg.wParam*/0;
}



//
//  FUNCIÓN: MyRegisterClass()
//
//  PROPÓSITO: Registra la clase de ventana.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MONG));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = 0;// MAKEINTRESOURCEW(IDC_MONG);
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCIÓN: InitInstance(HINSTANCE, int)
//
//   PROPÓSITO: Guarda el identificador de instancia y crea la ventana principal
//
//   COMENTARIOS:
//
//        En esta función, se guarda el identificador de instancia en una variable común y
//        se crea y muestra la ventana principal del programa.
//
/*jsValue scr_exit (jsExecState es, void* param) {
    if (jsArgCount(es) == 1 && jsIsNumber(jsArg(es, 0)))
        ExitProcess(jsToInt(es, jsArg(es, 0)));
    else
        ExitProcess(0);
}

jsValue scr_msgbox (jsExecState es, void* param) {
    if (jsArgCount(es) > 0 && jsArgType(es, 0)==jsType::JSTYPE_STRING)
        MessageBoxA(NULL,jsToString(es, jsArg(es, 0)), jsToString(es, jsArg(es, 1)),MB_OK);//?
    return jsNull();
}

jsValue scr_get_hwnd(jsExecState es, void* param) {
    return jsInt((int)g_hWnd);
}

jsValue scr_minimize(jsExecState es, void* param) {
    PostMessage(g_hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
    return jsNull();
}

jsValue scr_restore(jsExecState es, void* param) {
    PostMessage(g_hWnd, WM_SYSCOMMAND, SC_RESTORE, 0);
    return jsNull();
}

jsValue scr_maximize(jsExecState es, void* param) {
    PostMessage(g_hWnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
    return jsNull();
}

jsValue src_Stitle(jsExecState es, void* param) {
    LOG("js arg count: %d, type: %d -> %d", jsArgCount(es), jsArgType(es, 0), jsToInt(es,jsArg(es,0)));
    //SetWindowText(g_hWnd)
    //return jsNull();
    return jsNull();
}
jsValue src_Gtitle(jsExecState es, void* param) {
    LOG("js arg count: %d, type: %d ", jsArgCount(es), jsArgType(es, 0));
    return jsInt(666);
}*/


jsValue cb_hid_enumerate(jsExecState es, void* param) {
    jsValue ret = jsEmptyArray(es);
    int vid = NULL;
    int pid = NULL;
    if (jsArgCount(es) > 0 && jsArgType(es, 0) == JSTYPE_NUMBER) vid = jsToInt(es, jsArg(es, 0));
    if (jsArgCount(es) > 1 && jsArgType(es, 1) == JSTYPE_NUMBER) pid = jsToInt(es, jsArg(es, 1));
    hid_device_info* device = hid_enumerate(vid, pid);
    int i = 0;
    while (device != NULL) {
        //LOGW(L"HID DEVICE FOUND [%x:%x] %s -> %s", device->vendor_id, device->product_id, device->manufacturer_string, device->product_string);
        jsValue ret_obj = jsEmptyObject(es);
        jsSet(es, ret_obj, "vid", jsInt(device->vendor_id));
        jsSet(es, ret_obj, "pid", jsInt(device->product_id));
        jsSet(es, ret_obj, "serial", jsStringW(es, device->serial_number));
        jsSet(es, ret_obj, "product", jsStringW(es, device->product_string));
        jsSet(es, ret_obj, "manufacturer", jsStringW(es, device->manufacturer_string));
        jsSet(es, ret_obj, "path", jsString(es, device->path));
        jsSet(es, ret_obj, "interface", jsInt(device->interface_number));
        jsSet(es, ret_obj, "release", jsInt(device->release_number));
        jsSet(es, ret_obj, "usage", jsInt(device->usage));
        jsSet(es, ret_obj, "page", jsInt(device->usage_page));
        device = device->next;
        jsSetAt(es, ret, i, ret_obj); i++;
    }
    hid_free_enumeration(device);
    return ret;
}

jsValue cb_hid_open(jsExecState es, void* param) {
    int vid = NULL;
    int pid = NULL;
    wchar_t* serial = NULL;
    if (jsArgCount(es) > 0 && jsArgType(es, 0) == JSTYPE_NUMBER) vid = jsToInt(es, jsArg(es, 0));
    if (jsArgCount(es) > 1 && jsArgType(es, 1) == JSTYPE_NUMBER) pid = jsToInt(es, jsArg(es, 1));
    if (jsArgCount(es) > 2 && jsArgType(es, 2) == JSTYPE_STRING) serial = (wchar_t*)jsToStringW(es, jsArg(es, 2));
    hid_device* dev = hid_open(vid, pid, serial);
    hid_set_nonblocking(dev, TRUE);
    return jsInt((int)dev);
}

jsValue cb_hid_close(jsExecState es, void* param) {
    if (jsArgCount(es) == 1 && jsArgType(es, 0) == JSTYPE_NUMBER) {
        hid_device* dev = (hid_device*)jsToInt(es, jsArg(es, 0));
        if (dev == nullptr) return jsNull();
        hid_close(dev);
    }
    return jsNull();
}

jsValue cb_hid_read(jsExecState es, void* param) {
    int ret = 0;
    if (jsArgCount(es) >= 2 && jsArgType(es, 0) == JSTYPE_NUMBER && jsArgType(es, 1) == JSTYPE_NUMBER) {
        hid_device* dev = (hid_device*)jsToInt(es, jsArg(es, 0));
        if (dev == nullptr) return jsNull();
        int len = jsToInt(es, jsArg(es, 0));
        unsigned char* data = (unsigned char*)malloc(len);
        if (jsArgCount(es) == 3 && jsArgType(es, 2) == JSTYPE_NUMBER)
            ret = hid_read_timeout(dev, data, len, jsToInt(es, jsArg(es, 2)));
        else
            ret = hid_read(dev, data, len);
        if (ret > 0)
            return jsArrayBuffer(es, (char*)data, len);
    }
    return jsNull();
}

jsValue cb_hid_write(jsExecState es, void* param) {
    if (jsArgCount(es) >= 2 && jsArgType(es, 0) == JSTYPE_NUMBER && jsArgType(es, 1) == JSTYPE_ARRAY) {
        hid_device* dev = (hid_device*)jsToInt(es, jsArg(es, 0));
        if (dev == nullptr) return jsNull();
        wkeMemBuf* mbf = jsGetArrayBuffer(es, jsArg(es, 1));
        hid_write(dev, (unsigned char*)mbf->data, mbf->length);
    }
    return jsNull();
}


void cb_documentready(wkeWebView webView, void* param) {
    //LOG(_);
    //wchar_t DllPath[MAX_PATH];
}

bool cb_onloadurlbegin(wkeWebView webView, void* param, const utf8* url, wkeNetJob job) {
    LOG("url %s", url);
    if (startsWith("http://resources/", url)) {
        //wkeNetHookRequest(job);
        if (!PHYSFS_exists((char*)&url[17])) return FALSE;
        //LOG("url %s", (char*)&url[17]);
        PHYSFS_file* myfile = PHYSFS_openRead((char*)&url[17]);
        PHYSFS_sint64 file_size = PHYSFS_fileLength(myfile);
        char *myBuf = new char[file_size];
        int length_read = PHYSFS_read(myfile, myBuf, 1, PHYSFS_fileLength(myfile));
        //wkeNetSetMIMEType(job, "text/html");
        wkeNetSetData(job, myBuf, file_size);
        PHYSFS_close(myfile);
        return FALSE;
    }
    return TRUE;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
#define MAPWIDTH  800
#define MAPHEIGHT 600
    hInst = hInstance;
    wkeInitialize();
    const int ScreenX = (GetSystemMetrics(SM_CXSCREEN) - MAPWIDTH) / 2;
    const int ScreenY = (GetSystemMetrics(SM_CYSCREEN) - MAPHEIGHT) / 2;
    HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, ScreenX, ScreenY, MAPWIDTH, MAPHEIGHT, nullptr, nullptr, hInstance, nullptr);
    //CenterWindow(hWnd);
    if (!hWnd)
        return FALSE;
    g_hWnd = hWnd;
    RECT rcObRect;
    GetClientRect(hWnd, &rcObRect);
    window = wkeCreateWebWindow(WKE_WINDOW_TYPE_CONTROL, hWnd, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top);
    //wkeSetMediaPlayerFactory(window, cb_wkeMediaPlayerFactory, cb_wkeOnIsMediaPlayerSupportsMIMEType);
    wchar_t DllPath[MAX_PATH] = { 0 };
    GetModuleFileName(NULL, DllPath, sizeof(DllPath));
    wchar_t DllPath_dir[MAX_PATH] = { 0 };
    wchar_t DllPath_drive[20] = { 0 };
    _wsplitpath_s(DllPath, DllPath_drive, sizeof(DllPath_drive) / 2, DllPath_dir, sizeof(DllPath_dir) / 2, NULL, 0, NULL, 0);
    wsprintf(gPath, L"%s%s", DllPath_drive, DllPath_dir);
    wsprintf(DllPath, L"%sfront_end\\inspector.html", gPath);
    if (PathFileExists(DllPath))
        wkeShowDevtools(window, DllPath, NULL, NULL);
    wkeSetLanguage(window, "ES-es");
    /*wkeJsBindFunction("exit", scr_exit, NULL, NULL);
    wkeJsBindFunction("msg", scr_msgbox, NULL, NULL);
    wkeJsBindFunction("get_hwnd", scr_get_hwnd, NULL, NULL);
    wkeJsBindFunction("minimize", scr_minimize, NULL, NULL);
    wkeJsBindFunction("maximize", scr_maximize, NULL, NULL);
    wkeJsBindFunction("restore", scr_restore, NULL, NULL);*/
    wkeJsBindFunction("hid_open", cb_hid_open, NULL, NULL);
    wkeJsBindFunction("hid_close", cb_hid_close, NULL, NULL);
    wkeJsBindFunction("hid_enumerate", cb_hid_enumerate, NULL, NULL);
    wkeJsBindFunction("hid_read", cb_hid_read, NULL, NULL);
    wkeJsBindFunction("hid_write", cb_hid_write, NULL, NULL);
    wkeOnLoadUrlBegin(window, cb_onloadurlbegin, NULL);
    wkeSetContextMenuEnabled(window, FALSE);
    wkeSetCspCheckEnable(window, FALSE);
    wkeSetDragEnable(window, FALSE);
    wkeOnDocumentReady(window, cb_documentready, NULL);
    //wsprintf(DllPath, L"%s%sui.html", DllPath_drive, DllPath_dir);
    //wkeLoadURLW(window, DllPath);
    wkeLoadURLW(window, L"http://resources/wow.html");
    wkeShowWindow(window, TRUE);
    wkeEnableWindow(window, TRUE);
    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;
}

//
//  FUNCIÓN: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PROPÓSITO: Procesa mensajes de la ventana principal.
//
//  WM_COMMAND  - procesar el menú de aplicaciones
//  WM_PAINT    - Pintar la ventana principal
//  WM_DESTROY  - publicar un mensaje de salida y volver
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // Analizar las selecciones de menú:
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    break;
    case WM_SIZE:
        if (window != NULL) {
            RECT rc;
            GetClientRect(hWnd, &rc);
            wkeResize(window, rc.right, rc.bottom);
        }
        break;
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        // TODO: Agregar cualquier código de dibujo que use hDC aquí...
        EndPaint(hWnd, &ps);
    }
    break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Controlador de mensajes del cuadro Acerca de.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void Log(const char * szFormat, ...)
{
    char* tmp_buf = (char*)malloc(1024);
    va_list args;
    va_start(args, szFormat);
    vsprintf(tmp_buf, szFormat, args);
    va_end(args);
    printf(tmp_buf);
    OutputDebugStringA(tmp_buf);
    delete tmp_buf;
}

void Logw(const wchar_t * szFormat, ...)
{
    wchar_t* tmp_bufw = (wchar_t*)malloc(1024 * 2);
    va_list args;
    va_start(args, szFormat);
    vswprintf(tmp_bufw, szFormat, args);
    va_end(args);
    wprintf(tmp_bufw);
    OutputDebugStringW(tmp_bufw);
    delete tmp_bufw;
}